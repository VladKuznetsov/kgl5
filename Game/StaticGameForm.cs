﻿using App.DrawModels.BikeDrawModel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Game
{
    public partial class StaticGameForm : Form
    {
        private BikePainter bikePainter;
        private Bitmap bm;


        public StaticGameForm()
        {
            InitializeComponent();
            bikePainter = new BikePainter(300, 100, 0, 1, 10);
        }


        private void CustomPaint()
        {
            Bitmap bm = new Bitmap(pictureBox1.Width, pictureBox1.Height);
            bikePainter.BitMap = bm;
            bikePainter.DrawBike();
            pictureBox1.Image = bm;
        }


        
        private void StaticGameForm_KeyDown_1(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.W) bikePainter.MoveUp();
            if (e.KeyData == Keys.D) bikePainter.MoveLeft();
            if (e.KeyData == Keys.A) bikePainter.MoveLeft();
            if (e.KeyData == Keys.S) bikePainter.MoveDown();
            if (e.KeyData == Keys.Q) bikePainter.RotateRight();
            if (e.KeyData == Keys.E) bikePainter.RotateLeft();
            if (e.KeyData == Keys.R) bikePainter.ScaleUp();
            if (e.KeyData == Keys.T) bikePainter.ScaleDown();
            CustomPaint();
        }



        private void pictureBox1_Paint(object sender, PaintEventArgs e)
        {
            CustomPaint();
        }
    }
}
