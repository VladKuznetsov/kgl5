﻿namespace Game
{
    partial class NewGameForm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonNewStaticGame = new System.Windows.Forms.Button();
            this.buttonNewDynamicGame = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // buttonNewStaticGame
            // 
            this.buttonNewStaticGame.Location = new System.Drawing.Point(34, 33);
            this.buttonNewStaticGame.Name = "buttonNewStaticGame";
            this.buttonNewStaticGame.Size = new System.Drawing.Size(251, 109);
            this.buttonNewStaticGame.TabIndex = 0;
            this.buttonNewStaticGame.Text = "Static Game";
            this.buttonNewStaticGame.UseVisualStyleBackColor = true;
            this.buttonNewStaticGame.Click += new System.EventHandler(this.buttonNewStaticGame_Click);
            // 
            // buttonNewDynamicGame
            // 
            this.buttonNewDynamicGame.Location = new System.Drawing.Point(34, 170);
            this.buttonNewDynamicGame.Name = "buttonNewDynamicGame";
            this.buttonNewDynamicGame.Size = new System.Drawing.Size(251, 109);
            this.buttonNewDynamicGame.TabIndex = 1;
            this.buttonNewDynamicGame.Text = "Dynamic Game";
            this.buttonNewDynamicGame.UseVisualStyleBackColor = true;
            this.buttonNewDynamicGame.Click += new System.EventHandler(this.buttonNewDynamicGame_Click);
            // 
            // NewGameForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(331, 329);
            this.Controls.Add(this.buttonNewDynamicGame);
            this.Controls.Add(this.buttonNewStaticGame);
            this.Name = "NewGameForm";
            this.Text = "New Game";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonNewStaticGame;
        private System.Windows.Forms.Button buttonNewDynamicGame;
    }
}

