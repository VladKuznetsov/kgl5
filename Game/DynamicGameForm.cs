﻿using App.DrawModels.BikeDrawModel;
using App.DrawModels.BushDrawModel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Game
{
    public partial class DynamicGameForm : Form
    {
        private BikePainter bikePainter;
        private BushPainter bushBackFirst;
        private BushPainter bushBackSecond;
        private BushPainter bushFront;
        private Bitmap bm;
        private Timer timer;



        public DynamicGameForm()
        {
            InitializeComponent();
            bikePainter = new BikePainter(750, 50, -40, 1, 10);
            bushFront = new BushPainter(40, 400, -50, 1, 20);
            bushBackFirst = new BushPainter(360, 300, -50, 1, 20);
            bushBackSecond = new BushPainter(330, 300, -50, 1, 20);
            
        }


        private void DrawScence()
        {
            Bitmap bm = new Bitmap(pictureBox1.Width, pictureBox1.Height);
            bushFront.BitMap = bm;
            bushFront.DrawBush();
            bikePainter.MoveDown();
            bikePainter.BitMap = bm;
            bikePainter.DrawBike();
            bushBackFirst.BitMap = bm;
            bushBackFirst.DrawBush();
            bushBackSecond.BitMap = bm;
            bushBackSecond.DrawBush();
            pictureBox1.Image = bm;
        }



        private void buttonStart_Click(object sender, EventArgs e)
        {
            timer = new Timer();
            timer.Interval = 100;
            timer.Tick += Handler;
            timer.Start();
        }


        private void Handler(object sender, EventArgs e)
        {
            if (label1.Location.X > -1000)
            {
                label1.Location = new Point
                {
                    X = label1.Location.X - 100,
                    Y = label1.Location.Y
                };
            }
            else
            {
                label1.Location = new Point
                {
                    X = 1000,
                    Y = label1.Location.Y
                };
            }

            if (bikePainter.X > -300)
            {
                bikePainter.MoveLeft();
                DrawScence();
            }
            else
            {
                timer.Stop();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            bikePainter.MoveLeft();
            DrawScence();
        }

        private void DynamicGameForm_Load(object sender, EventArgs e)
        {
            DrawScence();
        }

        
    }
}
