﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Game
{
    public partial class NewGameForm : Form
    {
        public NewGameForm()
        {
            InitializeComponent();
        }

        private void buttonNewStaticGame_Click(object sender, EventArgs e)
        {
            var f = new StaticGameForm();
            f.Show();
        }

        private void buttonNewDynamicGame_Click(object sender, EventArgs e)
        {
            var f = new DynamicGameForm();
            f.Show();
        }
    }
}
