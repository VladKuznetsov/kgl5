﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.DrawModels.BikeDrawModel.Details
{
    public class Body
    {

        public Graphics g;
        public Bitmap bm;
        public Pen Pen { get; set; }
        private Line botomLine;
        public Line BotomLine { get { return botomLine; } }
        private Line topLine;
        public Line TopLine { get { return topLine; } }
        private Line middleLine;
        public Line MiddleLine { get { return middleLine; } }
        private Line frontLine;
        public Line FrontLine { get { return frontLine; } }



      
        private Line NewBotomModel(int x, int y, int step)
        {
            return new Line
            {
                LinePointFrom = new Point
                {
                    X = x,
                    Y = y
                },
                LinePointTo = new Point
                {
                    X = x - 4 * step,
                    Y = y
                }
            };
        }



        private Line NewMiddleLine(int x, int y, int step)
        {
            return new Line
            {
                LinePointFrom = new Point
                {
                    X = x - 4 * step,
                    Y = y
                },
                LinePointTo = new Point
                {
                    X = x - 8 * step,
                    Y = y - 4 * step
                }
            };
        }



        private Line NewFrontLine(int x, int y, int step)
        {
            return new Line
            {
                LinePointFrom = new Point
                {
                    X = x - 8 * step,
                    Y = y - 4 * step
                },
                LinePointTo = new Point
                {
                    X = x - 10 * step,
                    Y = y
                }
            };
        }



        private Line NewTopLine(int x, int y, int step)
        {
            return new Line
            {
                LinePointFrom = new Point
                {
                    X = x,
                    Y = y
                },
                LinePointTo = new Point
                {
                    X = x - 8 * step,
                    Y = y - 4 * step
                }
            };
        }



        public void BuildModel(int x, int y, int step)
        {
            botomLine = NewBotomModel(x, y, step);
            middleLine = NewMiddleLine(x, y, step);
            topLine = NewTopLine(x, y, step);
            frontLine = NewFrontLine(x, y, step);
        }



        public void DrawModel(Bitmap bm, int nx, int ny, int rotate, float scale, int step)
        {
            this.bm = bm;
            g = Graphics.FromImage(this.bm);
            g.TranslateTransform(nx, ny);
            g.RotateTransform(rotate);
            g.ScaleTransform(scale, scale);
            g.DrawLine(Pen, botomLine.LinePointFrom, botomLine.LinePointTo);
            g.DrawLine(Pen, middleLine.LinePointFrom, middleLine.LinePointTo);
            g.DrawLine(Pen, topLine.LinePointFrom, topLine.LinePointTo);
            g.DrawLine(Pen, frontLine.LinePointFrom, frontLine.LinePointTo);
        }
    }
}
