﻿using System.Drawing;

namespace App.DrawModels.BikeDrawModel.Details
{
    public class Wheel
    {
        public Graphics g;
        public Bitmap bm;
        public Pen Pen { get; set; }
        private Rectangle wheelContainer;
        public Rectangle WheelContainer { get { return wheelContainer; } }
        private Point point;
        public Point WheelPoint { get { return point; } }
        private Size size;
        public Size WheelSize { get { return size; } }


      

        public void BuildModel(int x, int y, int step)
        {
            int width = 4 * step;
            int heigth = 4 * step;
            point = new Point
            {
                X = x - width / 2,
                Y = y - heigth / 2
            };
            size = new Size
            {
                Width = width,
                Height = heigth
            };
            wheelContainer = new Rectangle(point, size);
        }



        public void DrawModel(Bitmap bm, int nx, int ny, int rotate, float scale, int step)
        {
            this.bm = bm;
            g = Graphics.FromImage(this.bm);
            g.TranslateTransform(nx, ny);
            g.RotateTransform(rotate);
            g.ScaleTransform(scale, scale);
            g.DrawEllipse(Pen, wheelContainer); 
        }
    }
}
