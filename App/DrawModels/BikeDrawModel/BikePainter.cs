﻿using App.DrawModels.BikeDrawModel.Details;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.DrawModels.BikeDrawModel
{
    public class BikePainter
    {
        public Pen Pen { get; set; }
        public Bitmap BitMap { get; set; }
        private Body bikeBody;
        public Body BikeBody { get { return bikeBody; } }
        private Wheel backWheel;
        public Wheel BackWheel { get { return backWheel; } }
        private Wheel frontWheel;
        public Wheel FrontWheel { get { return frontWheel; } }
        private int x;
        private int y;
        private int step;
        private int rotate;
        private float scale;
        public int X { get { return x; } }
        public int Y { get { return y; } }
        public int Step { get { return step; } }
        public int Rotate { get { return rotate; } }
        public float Scale { get { return scale; } }



        public BikePainter(int x, int y, int rotate, float scale, int step)
        {
            Pen = new Pen(Color.Red);
            Pen.Width = 5;
            this.scale = scale;
            this.rotate = rotate;
            this.x = x;
            this.y = y;
            this.step = step;
            Init();
        }



        private void Init()
        {
            bikeBody = new Body();
            backWheel = new Wheel();
            frontWheel = new Wheel();
            bikeBody.BuildModel(x, y, step);
            bikeBody.Pen = Pen;
            backWheel.BuildModel(bikeBody.BotomLine.LinePointFrom.X, bikeBody.BotomLine.LinePointFrom.Y, step);
            backWheel.Pen = Pen;
            frontWheel.BuildModel(bikeBody.FrontLine.LinePointTo.X, bikeBody.FrontLine.LinePointTo.Y, step);
            frontWheel.Pen = Pen;
        }



        public void DrawBike()
        {
            bikeBody.DrawModel(BitMap, x, y, rotate, scale, step);
            backWheel.DrawModel(BitMap, x, y, rotate, scale, step);
            frontWheel.DrawModel(BitMap, x, y, rotate, scale, step);
        }



        public void MoveLeft()
        {
            x -= 10;
        }



        public void MoveRigth()
        {
            x += 10;
        }



        public void MoveUp()
        {
            y -= 10;
        }



        public void MoveDown()
        {
            y += 10;
        }



        public void RotateRight()
        {
            rotate += 10;
        }


        public void RotateLeft()
        {
            rotate -= 10;
        }



        public void ScaleUp()
        {
            scale *= 2;
        }



        public void ScaleDown()
        {
            scale /= 2;
        }
    }
}
