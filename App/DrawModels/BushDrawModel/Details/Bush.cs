﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.DrawModels.BushDrawModel
{
    public class Bush
    {
        public Graphics g;
        public Bitmap bm;
        public Pen Pen { get; set; }
        private Line leftBranch;
        public Line LeftBranch { get { return leftBranch; } }
        private Line rightBranch;
        public Line RightBranch { get { return rightBranch; } }
        private Line middleBranch;
        public Line MiddleBranch { get { return middleBranch; } }

        private Line NewMiddleLine(int x, int y, int step)
        {
            return new Line
            {
                LinePointFrom = new Point
                {
                    X = x,
                    Y = y 
                },
                LinePointTo = new Point
                {
                    X = x,
                    Y = y - 4 * step
                }
            };
        }



        private Line NewLeftLine(int x, int y, int step)
        {
            return new Line
            {
                LinePointFrom = new Point
                {
                    X = x,
                    Y = y
                },
                LinePointTo = new Point
                {
                    X = x - 2 * step,
                    Y = y - 3 * step
                }
            };
        }



        private Line NewRightLine(int x, int y, int step)
        {
            return new Line
            {
                LinePointFrom = new Point
                {
                    X = x,
                    Y = y
                },
                LinePointTo = new Point
                {
                    X = x + 2 * step,
                    Y = y - 3 * step
                }
            };
        }



        public void BuildModel(int x, int y, int step)
        {
            middleBranch = NewMiddleLine(x, y, step);
            leftBranch = NewLeftLine(x, y, step);
            rightBranch = NewRightLine(x, y, step);
        }



        public void DrawModel(Bitmap bm, int nx, int ny, int rotate, float scale, int step)
        {
            this.bm = bm;
            g = Graphics.FromImage(this.bm);
            g.TranslateTransform(nx, ny);
            g.RotateTransform(rotate);
            g.ScaleTransform(scale, scale);
            g.DrawLine(Pen, middleBranch.LinePointFrom, middleBranch.LinePointTo);
            g.DrawLine(Pen, leftBranch.LinePointFrom, leftBranch.LinePointTo);
            g.DrawLine(Pen, rightBranch.LinePointFrom, rightBranch.LinePointTo);
        }
    }
}
