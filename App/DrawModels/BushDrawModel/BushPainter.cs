﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.DrawModels.BushDrawModel
{
    public class BushPainter
    {
        public Pen Pen { get; set; }
        public Bitmap BitMap { get; set; }
        private Bush bush;
        public Bush DBush { get { return bush; } }
        private int x;
        private int y;
        private int step;
        private int rotate;
        private float scale;
        public int X { get { return x; } }
        public int Y { get { return y; } }
        public int Step { get { return step; } }
        public int Rotate { get { return rotate; } }
        public float Scale { get { return scale; } }



        public BushPainter(int x, int y, int rotate, float scale, int step)
        {
            Pen = new Pen(Color.Blue);
            Pen.Width = 3;
            this.scale = scale;
            this.rotate = rotate;
            this.x = x;
            this.y = y;
            this.step = step;
            Init();
        }



        private void Init()
        {
            bush = new Bush();
            bush.BuildModel(x, y, step);
            bush.Pen = Pen;
        }



        public void DrawBush()
        {
            bush.DrawModel(BitMap, x, y, rotate, scale, step);
        }



        public void MoveRigth()
        {
            x -= 10;
        }



        public void MoveLeft()
        {
            x += 10;
        }



        public void MoveUp()
        {
            y -= 10;
        }



        public void MoveDown()
        {
            y += 10;
        }



        public void RotateRight()
        {
            rotate += 10;
        }


        public void RotateLeft()
        {
            rotate -= 10;
        }



        public void ScaleUp()
        {
            scale *= 2;
        }



        public void ScaleDown()
        {
            scale /= 2;
        }
    }
}
