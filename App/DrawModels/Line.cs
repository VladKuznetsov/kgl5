﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.DrawModels
{
    public class Line
    {
        public Point LinePointFrom { get; set; } 
        public Point LinePointTo { get; set; }
    }
}
